# D-Major Scale
D_major scale is just like C-Major scale except that it begins with the D note. The most important thing to notice when learning D-Major scale after C-Major (or any Major scale, as a matter of fact) is the pattern it follows is exactly the same.

To be more clear, here is the pattern:
1-2-3-4-5-6-7-1'

where 1 is D on which you start and 1' is the next D.

Just like C-Major scale, the 3-4 and the 7-1' notes are adjescent.