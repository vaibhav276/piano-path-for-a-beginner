# C-Major Scale

Ok, so what can be the simplest thing to learn on a piano? Well, of course it can be anything like playing a single note randomly anywhere on the 88 keys. But, remember the [music primer](some-music-primer.md)?

Now, what can you learn today that makes some sense when you play it? Its the C-Major scale. I think its the easiest scale because it involves all white keys, (no sharp or flat notes). So you don't have to know sharp and flat notes for playing C-Major scale.

To play the C-Major scale, you must start with, guess what?, C. And C is played on piano by any key that is immediately before a group of 2 black keys. But I'd recommend in the beginning to play with a C which is closest to your right hand when you are placing your hand most relaxed on the piano. For most, its the 3rd or 4th C from the right.

This is how it should sound like: http://upload.wikimedia.org/wikipedia/commons/5/59/C_major_scale.ogg

To play it, start with C and go all the way to the next C with all white keys, in a steady and comfortable flow.

>Fingering tip: There is no fixed finger position, but if you start from your thumb and use thumb again somewhere around 4th or 5th note, then it will be probably easy.

## What just happened?
The C-Major scale, like all other scales, consists of the following pattern:
1-2-3-4-5-6-7-1'

Where 1 is the C you start with and 1' is the next C.

With some repetition, it comes natural to mentally link the 3-4 and 7-1' as the only group of notes that are adjescent. This characterizes a major scale.

So, think of C-Major scale as a major scale that begins with C.