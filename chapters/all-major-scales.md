# All Major Scales
Having known the C-Major scale and D-Major scale, it is easy to go ahead and play any Major scale. Only one thing needs to be remembered - In a major scale, the 3-4 and 7-1' are adjescent notes.

Here is a quick reference for all major scales:

	A-Major:  A  B  C# D  E  F# G# A
	A#-Major: A# C  D  D# F  G  A  A#
	B-Major:  B  C# D# E  F# G# A# B
	C-Major:  C  D  E  F  G  A  B  C
	C#-Major: C# D# F  F# G# A# C  C#
	D-Major:  D  E  F# G  A  B  C# D
	D#-Major: D# F  G  G# A# C  D  D#
    E-Major:  E  F# G# A  B  C# D# E
    F-Major:  F  G  A  A# C  D  E  F
    F#-Major: F# G# A# B  C# D# F  F#
    G-Major:  G  A  B  C  D  E  F# G
    G#-Major: G# A# C  C# D# F  G  G#

A brief practice with all or some of these scales should lay a solid foundation to recognize a Major scale by ear.