# Sharps and Flats

The names of notes, in case you haven't noticed yet, are:
A, B, C, D, E, F, G

That's it. After G, the next one is again A.

But that does not cover all the notes that can be played on a piano. These are only the ones that can be played by white keys. The ones played on black keys are the sharps and flat notes. (The normal ones are called naturals)

I like to think of sharps and flats as special notes for the naturals. Because they give a special meaning to the naturals that they lie next to.

For example, there is B-flat, which is the note immediately before B natural. It gives a special meaning to B. The same note can also be called as A-sharp because it immediately follows the natural A note. So this note is a softer version of the B note and a sharper version of the A note, as it lies in between A and B.

If you look at the piano, you will notice that for some notes, there are no sharps or flats. For example, there is nothing between B and C. Similarly, there is nothing between E and F. What this means is, if someone tells you to play F-flat, either he's referring to E (he's extra smart)  or he doesn't know what he's talking about. So, your answer should be to play E.
