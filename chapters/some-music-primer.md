# Some music primer

Music is a combination of three things (if not more):
- Melody
- Harmony
- Time

**Melody**, to me, is simply a sequence of sound pulses that make sense together in a sequence.

We say two or more sound pulses are in **harmony** if they make sense when played together ( at the same time ). 

**Time** is the string on which we tie together the melody and harmony so that, again, the result makes sense.

You might be wondering, what is the "*sense*" that I am referring to in all the three definitions. Well, sense is nothing but the feeling you get when you listen to music. That's the purpose of music. Always remember what you're trying to do - if the music does not make sense, it's not music at all. Feel yourself as a chef, who is preparing a dish. If the dish does not taste as you want it to, its not a dish.

Thats about all you need to know for starting to learn music. The next time you listen to any music, think of these definitions and try to imagine yourself as the musician who created it. And listen to as many pieces of music you have time for, and listen to them repeatidly. It will develop your inner musician, the one who is going to actually teach you to play music.