# Summary

* [Introduction](README.md)
* [Some music primer](chapters/some-music-primer.md)
* [C-Major Scale](chapters/c-major-scale.md)
* [Sharps and Flats](chapters/sharps-and-flats.md)
* [D-Major Scale](chapters/d-major-scale.md)
* [All Major Scales](chapters/all-major-scales.md)
* [Where are the songs?](chapters/where-are-the-songs.md)
* C-Major Chord (Triad)
* First chord progression
* Both hands together