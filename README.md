# Piano - Path for a beginner
First-off, in the title of the book "Piano - Path for a beginner", the beginner I am referring to is myself.

This book is my own path for learning piano (on my own). 
I have found a lot of excellent and free material online to learn and master the technique of playing piano. Each of the sources have their own (valid) way of progressing, but occasionally I found some gaps that need to be filled for a beginner. More often, I found a lot of slow-paced information or spoon-feeding, which I am afraid may kill the enthusiasm of some people, specially very early into learning piano.
Therefore, I felt a need of a structured consolidation of all the material that I found useful and adding some fun element into it so that the process never gets boring.

This book is my attempt at building something which is real, fun-filled and free. I say real because it really is the same path I followed (am following). For you, the path can be totally different, or similar depending on your taste of music, your desire to learn some specific parts first, etc. But don't worry. I am not trying to alter your path to learning. I am just sharing my path as an experience. Take whatever you like from it, or follow it completely, whatever you wish.

Lets begin the journey!!

(Please remember that I am writing this book alongwith learning piano. It's a living piece of work and it is going to be dynamic)
